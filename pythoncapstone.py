from abc import ABC


class Person(ABC):

    def getFullName(self):
        pass
    
    def addRequest(self):
        pass
    
    def checkRequest(self):
        pass
    
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()

        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFirstName(self):
        return f"{self._firstName}"
    
    def setFirstName(self, firstName):
        self._firstName = firstName
    
    def getLastName(self):
        return f"{self._lastName}"
    
    def setLastName(self, lastName):
        self._lastName = lastName
    
    def getEmail(self):
        return f"{self._email}"
    
    def setEmail(self, email):
        self._email = email
    
    def getDepartment(self):
        return f"{self._department}"
    
    def setDepartment(self, department):
        self._department = department
    
    def checkRequest(self):
        pass
    
    def addUser(self):
        pass
    
    def addRequest(self):
        return f"Request has been added"
    
    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"
    
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"


class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()

        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    def getFirstName(self):
        return f"{self._firstName}"
    
    def setFirstName(self, firstName):
        self._firstName = firstName
    
    def getLastName(self):
        return f"{self._lastName}"
    
    def setLastName(self, lastName):
        self._lastName = lastName
    
    def getEmail(self):
       return f"{self._email}"
    
    def setEmail(self, email):
        self._email = email
    
    def getDepartment(self):
        return f"{self._department}"
    
    def setDepartment(self, department):
        self._department = department
    
    def checkRequest(self):
        pass
    
    def addUser(self):
        pass
    
    def login(self):
        return f"{self._email} has logged in."

    def logout(self):
        return f"{self._email} has logged out"
    
    def addMember(self, employee):
        return self._members.append(employee)
    
    def get_members(self):
        return f"{self._members}"
    
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"


class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()

        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFirstName(self):
        return f"{self._firstName}"
    
    def setFirstName(self, firstName):
        self._firstName = firstName
    
    def getLastName(self):
       return f"{self._lastName}"
    
    def setLastName(self, lastName):
        self._lastName = lastName
    
    def getEmail(self):
       return f"{self._email}"
    
    def setEmail(self, email):
        self._email = email
    
    def getDepartment(self):
        return f"{self._department}"
    
    def setDepartment(self, department):
        self._department = department
    
    def checkRequest(self):
        pass
    
    def addRequest(self):
        pass
    
    def login(self):
        return f"{self._email} has logged in."
    
    def logout(self):
        return f"{self._email} has logged out."
    
    def addUser(self):
        return f"User has been added"

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"



class Request():

    def __init__(self, name, requester, dateRequested,):

        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = "Open"
    def getName(self):
        return f"{self.name}"
    
    def setName(self, name):
       self.name = name
    
    def getRequester(self):
        return f"{self.requester}"
    
    def setRequester(self, requester):
       self.requester = requester
    
    def getDateRequested(self):
        return f"{self.dateRequested}"
    
    def setDateRequested(self, dateRequested):
       self.dateRequested = dateRequested

    def getStatus(self):
        return f"{self.status}"
    
    def set_status(self, status):
       self.status = status

    def updateRequest(self):
        return f"{self.name} has been {self.status}."
    
    def closeRequest(self):
       return f"{self.name} has been {self.status}."
    
    def cancelRequest(self):
        return f"{self.name} has been {self.status}."


employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")



assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1._members:
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())





        

        

            

